# IndustrialCraft 2 Combo Armors #

IndustrialCraft 2 Combo Armors, or IC2CA, is an addon for the IndustrialCraft 2 mod for Minecraft. Remember all those times you wanted to use your Electric Jetpack (because who doesn't love to fly) but your Mining Laser was out of energy? Or maybe you wanted to recharge your Nano Saber and have some reasonable protection from a NanoSuit Chestplate at the same time? Well, as you may have guessed from my corny intro and the title, this mod adds many different items that allow you to do just that. You can combine all of the electrical armors from IC2 to create a wide variety of different armors, from the Solar QuantumSuit Helmet to the LapiJetpack to the Ultimate QuantumSuit Bodyarmor.

Also, currently in development is a cool upgrade system that will allow you to make your cool armors even cooler. From something as simple as a Solar Production module that will make a Solar Helmet more efficient, to something as cool as the Cloaking Module that allows you to turn invisible, we have a long list of upgrades.

Lastly, we added a Nano Bow to match the Nano Saber. Even though it doesn't fit with the whole combo armors thing, we felt it was necessary :).

## Credits ##

UnixRano: Project Head, Lead Coder, Artist, Design

MasterMan1010: Lead Artist, Design, Beta Tester

Ishaan1: Lead Wiki Design, Beta Tester, Test Server Owner

Kyogredude: Coder, Design, Beta Tester

DragnHntr: Lead Wiki Contributor, Wiki Moderator, Awesome Person

Zuxelus: Coder

### Forum ###

http://forum.industrial-craft.net/index.php?page=Thread&threadID=8861

###Downloads ###

Version ic2ca-1.6.4-1.14.3.08c.jar for IC2 1.118.401-lf (Minecraft 1.6.4)

[ic2ca-1.6.4-1.14.3.08c.jar](https://bitbucket.org/Zuxelus/ic2-combo-armors/downloads/ic2ca-1.6.4-1.14.3.08c.jar)

Version ic2ca-1.6.4-1.14.3.08.jar for IC2 2.0.397-experimental (Minecraft 1.6.4)

[ic2ca-1.6.4-1.14.3.08.jar](https://bitbucket.org/Zuxelus/ic2-combo-armors/downloads/ic2ca-1.6.4-1.14.3.08.jar)

Version ic2ca-1.7.2-1.14.4.03.jar for IC2 2.1.484-experimental (Minecraft 1.7.2)

[ic2ca-1.7.2-1.14.4.03.jar](https://bitbucket.org/Zuxelus/ic2-combo-armors/downloads/ic2ca-1.7.2-1.14.4.03.jar)

Version ic2ca-1.7.10-1.14.5.05.jar for IC2 2.2.645-experimental+ (Minecraft 1.7.10)

[ic2ca-1.7.10-1.14.5.05.jar](https://bitbucket.org/Zuxelus/ic2-combo-armors/downloads/ic2ca-1.7.10-1.14.5.05.jar)

Version ic2ca-1.7.10-1.14.5.06.jar for IC2 2.2.767-experimental+ (Minecraft 1.7.10)

[ic2ca-1.7.10-1.14.5.06.jar](https://bitbucket.org/Zuxelus/ic2-combo-armors/downloads/ic2ca-1.7.10-1.14.5.06.jar)
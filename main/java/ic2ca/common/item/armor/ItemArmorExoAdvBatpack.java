package ic2ca.common.item.armor;

import net.minecraft.entity.Entity;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemArmorExoAdvBatpack extends ItemArmorElectricUtility
{
	public ItemArmorExoAdvBatpack(int id, String name, int renderIndex)
	{
		super(id, name, renderIndex, 1, 600000, 1000, 2, true);
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		return "ic2ca:textures/armor/exoadvbatpack_1.png";
	}

	@Override
	public double getDamageAbsorptionRatio()
	{
		return 0.0D;
	}

	@Override
	public int getEnergyPerDamage()
	{
		return 0;
	}

	@Override
	public int getItemTier()
	{
		return 2;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack var1)
	{
		return EnumRarity.uncommon;
	}
}

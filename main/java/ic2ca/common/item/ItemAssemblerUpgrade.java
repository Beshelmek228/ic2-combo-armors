package ic2ca.common.item;

import ic2ca.common.IC2CA;
import ic2ca.common.Ic2caItems;
import ic2ca.common.Util;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemAssemblerUpgrade extends Item
{
	private int raritynum = 0;

	public ItemAssemblerUpgrade(int id, String name, int rarity)
	{
		super(id);
		if (name == "speedUpgrade") 
		{
			this.setMaxStackSize(4);
		}
		else 
		{
			this.setMaxStackSize(1);	
		}
		this.raritynum = rarity;
		this.setUnlocalizedName(name);
		this.setCreativeTab(IC2CA.tabIC2CA);
		GameRegistry.registerItem(this, name);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack is, EntityPlayer player, List list, boolean par4)
	{
		list.add(StatCollector.translateToLocal("info.compatableallassemblers"));
		if (is.isItemEqual(Ic2caItems.opUpgrade)) 
		{
			list.add(StatCollector.translateToLocal("info.instantlyprocessesarmors"));
		}
		else if (is.isItemEqual(Ic2caItems.speedBooster)) 
		{
			list.add(StatCollector.translateToLocal("info.reducesprocessingtime"));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack itemstack)
	{
		if (this.raritynum == 2) {
			return EnumRarity.uncommon;
		}
		if (this.raritynum == 3) {
			return EnumRarity.rare;
		}
		if (this.raritynum == 4) {
			return EnumRarity.epic;
		}
		return EnumRarity.common;
	}

	@Override
	public void registerIcons(IconRegister ir)
	{
		this.itemIcon = Util.register(ir, this);
	}
}

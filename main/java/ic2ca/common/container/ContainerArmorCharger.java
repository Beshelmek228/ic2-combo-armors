package ic2ca.common.container;

import ic2.core.block.machine.ContainerElectricMachine;
import ic2.core.slot.SlotInvSlot;
import ic2ca.common.tileentity.TileEntityArmorCharger;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;

public class ContainerArmorCharger extends ContainerElectricMachine
{
	public final TileEntityArmorCharger tileEntity;
	public ContainerArmorCharger(EntityPlayer player, TileEntityArmorCharger tileentity)
	{
	    super(player, tileentity, 166, 56, 53);		
		this.tileEntity = tileentity;
		/*addSlotToContainer(new SlotInvSlot(tileentity.inputSlot1, 0, 42, 17));
		addSlotToContainer(new SlotInvSlot(tileentity.inputSlot2, 0, 68, 17));
		addSlotToContainer(new SlotInvSlot(tileentity.outputSlot, 0, 122, 35));
		addSlotToContainer(new SlotInvSlot(tileentity.updateSlot, 0, 150, 35));*/
		bindPlayerInventory(player.inventory);
	}

	protected void bindPlayerInventory(InventoryPlayer inventoryPlayer)
	{
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; j++) {
				addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
			}
		}
		for (int i = 0; i < 9; i++) {
			addSlotToContainer(new Slot(inventoryPlayer, i, 8 + i * 18, 142));
		}
	}
}

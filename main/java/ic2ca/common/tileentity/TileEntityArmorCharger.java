package ic2ca.common.tileentity;

import ic2.core.ContainerBase;
import ic2.core.IHasGui;
import ic2.core.block.machine.tileentity.TileEntityElectricMachine;
import ic2ca.common.container.ContainerArmorCharger;
import ic2ca.common.gui.GuiArmorCharger;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.ForgeDirection;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileEntityArmorCharger extends TileEntityElectricMachine implements IHasGui
{	
	public TileEntityArmorCharger()
	{
		super(10000, 5, 2);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public GuiScreen getGui(EntityPlayer player, boolean isAdmin)
	{
		return new GuiArmorCharger(new ContainerArmorCharger(player, this));
	}

	@Override
	public ContainerBase getGuiContainer(EntityPlayer player)
	{
		return new ContainerArmorCharger(player, this);
	}

	@Override
	public String getInvName()
	{
		return "Armor Charger";
	}

	@Override
	public void onGuiClosed(EntityPlayer paramEntityPlayer) {}
}
package ic2ca.common;

import ic2.core.IC2;
import ic2.core.util.StackUtil;
import ic2ca.common.item.armor.IJetpack;
import ic2ca.common.item.armor.ItemArmorBaseJetpack;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;

public class CommonPacketHandleric2ca implements IPacketHandler
{
	public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player)
	{
		EntityPlayerMP entityPlayer = (EntityPlayerMP)player;
		ByteArrayInputStream bais = new ByteArrayInputStream(packet.data);
		DataInputStream stream = new DataInputStream(bais);

		double x = 0.0D;
		double y = 0.0D;
		double z = 0.0D;
		try
		{
			String tag = stream.readUTF();
			int var7;
			if (tag.equalsIgnoreCase("overcharge"))
			{
				ItemStack armor = entityPlayer.inventory.armorInventory[2];
				try
				{
					var7 = stream.readInt();
					x = stream.readDouble();
					y = stream.readDouble();
					z = stream.readDouble();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				if (armor != null && IC2CA.chests.contains(armor.getItem().itemID))
				{
					NBTTagCompound nbt = StackUtil.getOrCreateNbtData(armor);
					if (nbt.getBoolean("overcharge")) {
						UpgradesCommonProxy.overcharge(entityPlayer, armor, x, y, z);
					}
				}
			}
			if (tag.equalsIgnoreCase("flyToggle"))
			{
				ItemStack armor = entityPlayer.inventory.armorInventory[2];
				if (armor != null && armor.getItem() instanceof ItemArmorBaseJetpack)
				{
					NBTTagCompound nbt = StackUtil.getOrCreateNbtData(armor);
					if (nbt.getBoolean("flight"))
					{
						if (!IC2.keyboard.isModeSwitchKeyDown(entityPlayer) && !IC2.keyboard.isSneakKeyDown(entityPlayer)) 
						{
							UpgradesCommonProxy.switchFlyModeServer(entityPlayer, armor);
						}
					}
					else 
					{
					if (!IC2.keyboard.isModeSwitchKeyDown(entityPlayer) && !IC2.keyboard.isSneakKeyDown(entityPlayer))
					{
						IJetpack ij = (IJetpack)armor.getItem();
						ij.onFlykeyPressed(armor, entityPlayer);
					}
					}
				}
			}
			if (tag.equalsIgnoreCase("cloakToggle"))
			{
				ItemStack armor = entityPlayer.inventory.armorInventory[2];
				NBTTagCompound chestnbt = ServerTickHandleric2ca.getChestStored(entityPlayer);
				if (armor != null && IC2CA.chests.contains(armor.getItem().itemID))
				{
					NBTTagCompound nbt = StackUtil.getOrCreateNbtData(armor);
					if (nbt.getBoolean("cloaking")) {
						if (IC2.keyboard.isModeSwitchKeyDown(entityPlayer) && !IC2.keyboard.isSneakKeyDown(entityPlayer))
						{
							UpgradesCommonProxy.switchCloakModeServer(entityPlayer, armor);
						}
					}
				}
				else if (chestnbt != null && chestnbt.getBoolean("has"))
				{
					int id = chestnbt.getInteger("id");
					int damage = chestnbt.getInteger("damage");
					int size = chestnbt.getInteger("size");
					ItemStack is = new ItemStack(id, size, damage);
					if (chestnbt.getCompoundTag("nbt") != null)
					{
						NBTTagCompound nbt = chestnbt.getCompoundTag("nbt");
						is.setTagCompound(nbt);
					}
					if (is != null && IC2CA.chests.contains(is.getItem().itemID))
					{
						NBTTagCompound nbt = StackUtil.getOrCreateNbtData(is);
						if (nbt.getBoolean("cloaking")) 
						{
							if (IC2.keyboard.isModeSwitchKeyDown(entityPlayer) && !IC2.keyboard.isSneakKeyDown(entityPlayer))
							{
								UpgradesCommonProxy.switchCloakModeServer(entityPlayer, is);
							}
						}
					}
				}
			}
			if (tag.equalsIgnoreCase("worldLoad"))
			{
				ItemStack armor = entityPlayer.inventory.armorInventory[2];
				if (armor != null && armor.getItem() instanceof ItemArmorBaseJetpack)
				{
					NBTTagCompound nbt = StackUtil.getOrCreateNbtData(armor);
					if (nbt.getBoolean("flight")) 
					{
						UpgradesCommonProxy.firstLoadServer(entityPlayer, armor);
					}
				}
				NBTTagCompound chestnbt = ServerTickHandleric2ca.getChestStored(entityPlayer);
				if (armor != null && IC2CA.chests.contains(armor.getItem().itemID))
				{
					NBTTagCompound nbt = StackUtil.getOrCreateNbtData(armor);
					if (nbt.getBoolean("cloaking")) {
						UpgradesCommonProxy.firstLoadServer(entityPlayer, armor);
					}
				}
				else if (chestnbt != null && chestnbt.getBoolean("has"))
				{
					int id = chestnbt.getInteger("id");
					int damage = chestnbt.getInteger("damage");
					int size = chestnbt.getInteger("size");
					ItemStack is = new ItemStack(id, size, damage);
					if (chestnbt.getCompoundTag("nbt") != null)
					{
						NBTTagCompound nbt = chestnbt.getCompoundTag("nbt");
						is.setTagCompound(nbt);
					}
					if (is != null && IC2CA.chests.contains(is.getItem().itemID))
					{
						NBTTagCompound nbt = StackUtil.getOrCreateNbtData(is);
						if (nbt.getBoolean("cloaking")) 
						{
							UpgradesCommonProxy.firstLoadServer(entityPlayer, is);
						}
					}
				}
			}
			if (tag.equalsIgnoreCase("keyState"))
			{
				var7 = stream.readInt();
				Keyboardic2ca.processKeyUpdate(entityPlayer, var7);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}

package ic2ca.common.item;

import net.minecraft.util.StatCollector;

public enum EnumUpgradeType
{
  JETPACKS("info.jetpacks"),  
  SOLARS("info.solarhelmets"),  
  STATICS("info.staticboots"),  
  CHESTS("info.chestplates"),  
  HELMETS("info.helmets"),  
  ELECTRICS("info.electricalitems");
  
  public String name;
  
  private EnumUpgradeType(String s)
  {
    this.name = s;
  }
}

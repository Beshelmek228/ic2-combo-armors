package ic2ca.common;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class CreativeTabIC2CA extends CreativeTabs
{
  private static ItemStack ultimatequantum;
  
  public CreativeTabIC2CA()
  {
    super("IndustrialCraft 2 Combo Armors");
  }
  
  @Override
  @SideOnly(Side.CLIENT)
  public ItemStack getIconItemStack()
  {
    if (ultimatequantum == null) {
      ultimatequantum = Ic2caItems.quantumUltimate;
    }
    return ultimatequantum;
  }
  
  @Override
  @SideOnly(Side.CLIENT)
  public int getTabIconItemIndex()
  {
    return Ic2caItems.quantumUltimate.itemID;
  }
  
  @Override
  public String getTranslatedTabLabel()
  {
    return "IndustrialCraft 2 Combo Armors";
  }
  
}

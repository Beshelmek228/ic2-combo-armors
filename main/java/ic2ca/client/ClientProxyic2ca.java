package ic2ca.client;

import ic2ca.common.CommonProxyic2ca;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.world.WorldEvent.Load;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.client.registry.KeyBindingRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;

public class ClientProxyic2ca extends CommonProxyic2ca
{
	public static Minecraft mc = FMLClientHandler.instance().getClient();

	public static void registerKeys()
	{
		KeyBindingRegistry.registerKeyBinding(new KeyboardClientic2ca());
	}

	public static boolean sendMyPacket(String tag, int var1, EntityPlayer player)
	{
		ByteArrayOutputStream stream = new ByteArrayOutputStream(8);
		DataOutputStream datastream = new DataOutputStream(stream);
		double x = 0;
		double y = 0;
		double z = 0;
		if (tag.equals("overcharge"))
		{
			MovingObjectPosition mop = player.rayTrace(75.0D, 1.0F);
			if (mop != null)
			{
				Vec3 vec3 = mop.hitVec;
				x = vec3.xCoord;
				y = vec3.yCoord;
				z = vec3.zCoord;
			}
		}
		try
		{
			datastream.writeUTF(tag);
			datastream.writeInt(var1);
			if (tag.equals("overcharge"))
			{
				datastream.writeDouble(x);
				datastream.writeDouble(y);
				datastream.writeDouble(z);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		Packet250CustomPayload packet = new Packet250CustomPayload();
		packet.channel = "ic2ca";
		packet.data = stream.toByteArray();
		packet.length = stream.size();
		mc.thePlayer.sendQueue.addToSendQueue(packet);
		return true;
	}

	public static void sendPlayerMessage(EntityPlayer player, String message)
	{
		if (!mc.theWorld.isRemote)
		{
			player.addChatMessage(message);
		}
	}

	@Override
	public int addArmor(String armor)
	{
		return RenderingRegistry.addNewArmourRendererPrefix(armor);
	}

	public EntityPlayer getPlayerInstance()
	{
		return Minecraft.getMinecraft().thePlayer;
	}

	@ForgeSubscribe
	public void onWorldLoad(Load var1)
	{
		UpgradesClientProxy.firstLoad = true;
	}

	@Override
	public void registerThings()
	{
		TickRegistry.registerTickHandler(new ClientTickHandleric2ca(), Side.CLIENT);

		MinecraftForge.EVENT_BUS.register(this);
		registerKeys();
	}
}

package ic2ca.common.item.armor;

import ic2.api.item.IMetalArmor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemBodyNanoEnergypack extends ItemArmorElectricUtility implements IMetalArmor
{
	public ItemBodyNanoEnergypack(int id, String name, int renderIndex)
	{
		super(id, name, renderIndex, 1, 8000000, 1000, 4, true);
	}

	public boolean canProvideEnergy()
	{
		return true;
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		return "ic2ca:textures/armor/nanoenergy_1.png";
	}

	@Override
	public double getDamageAbsorptionRatio()
	{
		return 0.9D;
	}

	@Override
	public int getEnergyPerDamage()
	{
		return 800;
	}

	@Override
	public int getItemTier()
	{
		return 5;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack var1)
	{
		return EnumRarity.uncommon;
	}

	@Override
	public boolean isMetalArmor(ItemStack var1, EntityPlayer var2)
	{
		return true;
	}
}

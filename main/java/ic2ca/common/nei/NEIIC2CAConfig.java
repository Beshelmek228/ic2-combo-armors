package ic2ca.common.nei;

import ic2.core.Ic2Items;
import ic2ca.common.Ic2caItems;
import net.minecraft.item.ItemStack;
import codechicken.nei.MultiItemRange;
import codechicken.nei.api.API;
import codechicken.nei.api.IConfigureNEI;

public class NEIIC2CAConfig implements IConfigureNEI
{
	private static void addSubset(String name, Object... items)
	{
		MultiItemRange multiitemrange = new MultiItemRange();
		for (Object item : items) {
			if ((item instanceof ItemStack)) {
				multiitemrange.add((ItemStack)item);
			} else {
				multiitemrange.add(((Integer)item).intValue());
			}
		}
		API.addSetRange(name, multiitemrange);
	}

	public void addSubsets()
	{
		addSubset("IC2CA.Items", new Object[] { Integer.valueOf(Ic2caItems.nanoBow.itemID) });
	}

	@Override
	public String getName() {
		return "IndustrialCraft 2 Combo Armors";
	}

	@Override
	public String getVersion() {
		return "1.14.3.05";
	}

	@Override
	public void loadConfig() {
		API.registerRecipeHandler(new ArmorAssemblerRecipeHandler());
		API.registerUsageHandler(new ArmorAssemblerRecipeHandler());
		addSubsets();	    
	}

}  
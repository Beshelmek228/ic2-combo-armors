package ic2ca.common;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet250CustomPayload;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;

public class CommonProxyic2ca
{
  public static boolean sendPacket(EntityPlayer player, String tag, int var2)
  {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    DataOutputStream datastream = new DataOutputStream(stream);
    try
    {
      datastream.writeUTF(tag);
      datastream.writeInt(var2);
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    Packet250CustomPayload packet = new Packet250CustomPayload();
    packet.channel = "ic2ca";
    packet.data = stream.toByteArray();
    packet.length = packet.data.length;
    PacketDispatcher.sendPacketToPlayer(packet, (Player)player);
    return true;
  }
  
  public static void sendPlayerMessage(EntityPlayer player, String message)
  {
	  player.addChatMessage(message);
  }
  
  public int addArmor(String armor)
  {
    return 0;
  }
  
  public EntityPlayer getPlayerInstance()
  {
    return null;
  }
  
  public void registerThings() {}
  
  public void removePotion(EntityLivingBase var1, int var2)
  {
    var1.removePotionEffect(var2);
  }
}

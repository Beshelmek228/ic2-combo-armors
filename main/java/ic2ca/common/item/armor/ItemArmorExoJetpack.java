package ic2ca.common.item.armor;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemArmorExoJetpack extends ItemArmorBaseJetpack
{
	public ItemArmorExoJetpack(int id, String name, int renderIndex)
	{
		super(id, name, renderIndex,  1, 30000, 60, 1, false);
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		return "ic2ca:textures/armor/exojetpack_1.png";
	}

	@Override
	public int getItemTier()
	{
		return 1;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack var1)
	{
		return EnumRarity.common;
	}
	
	@Override
	public ArmorProperties getProperties(EntityLivingBase var1, ItemStack var2, DamageSource var3, double var4, int var6)
    {
        return super.getProperties(var1, var2, var3, var4, var6);
    }
}

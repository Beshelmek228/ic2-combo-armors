package ic2ca.common;

import ic2.core.util.StackUtil;
import ic2ca.common.item.armor.ItemArmorBaseJetpack;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

public class ServerTickHandleric2ca implements ITickHandler
{
  public static Map isFlyActiveByMod = new HashMap();
  public static Map isCloakActiveByMod = new HashMap();
  public static Map lastFlyUndressed = new HashMap();
  public static Map lastCloakUndressed = new HashMap();
  public static Map isLastCreativeState = new HashMap();
  public static Map helmetStored = new HashMap();
  public static Map pantsStored = new HashMap();
  public static Map bootsStored = new HashMap();
  public static Map chestStored = new HashMap();
  
  public static boolean checkCloakActiveByMode(EntityPlayer var0)
  {
    return isCloakActiveByMod.containsKey(var0) ? ((Boolean)isCloakActiveByMod.get(var0)).booleanValue() : false;
  }
  
  public static boolean checkFlyActiveByMode(EntityPlayer var0)
  {
    return isFlyActiveByMod.containsKey(var0) ? ((Boolean)isFlyActiveByMod.get(var0)).booleanValue() : false;
  }
  
  public static boolean checkLastCloakUndressed(EntityPlayer var0)
  {
    return lastCloakUndressed.containsKey(var0) ? ((Boolean)lastCloakUndressed.get(var0)).booleanValue() : false;
  }
  
  public static boolean checkLastCreativeState(EntityPlayer var0)
  {
    return isLastCreativeState.containsKey(var0) ? ((Boolean)isLastCreativeState.get(var0)).booleanValue() : false;
  }
  
  public static boolean checkLastFlyUndressed(EntityPlayer var0)
  {
    return lastFlyUndressed.containsKey(var0) ? ((Boolean)lastFlyUndressed.get(var0)).booleanValue() : false;
  }
  
  public static NBTTagCompound getBootsStored(EntityPlayer var0)
  {
    return bootsStored.containsKey(var0) ? (NBTTagCompound)bootsStored.get(var0) : null;
  }
  
  public static NBTTagCompound getChestStored(EntityPlayer var0)
  {
    return chestStored.containsKey(var0) ? (NBTTagCompound)chestStored.get(var0) : null;
  }
  
  public static NBTTagCompound getHelmetStored(EntityPlayer var0)
  {
    return helmetStored.containsKey(var0) ? (NBTTagCompound)helmetStored.get(var0) : null;
  }
  
  public static NBTTagCompound getPantsStored(EntityPlayer var0)
  {
    return pantsStored.containsKey(var0) ? (NBTTagCompound)pantsStored.get(var0) : null;
  }
  
  @Override
  public String getLabel()
  {
    return "ic2ca";
  }
  
  @Override
  public void tickEnd(EnumSet var1, Object... var2) {}
  
  @Override
  public EnumSet<TickType> ticks()
  {
    return EnumSet.of(TickType.WORLDLOAD, TickType.PLAYER);
  }
  
  @Override
  public void tickStart(EnumSet type, Object... tickData)
  {
    if (type.contains(TickType.PLAYER))
    {
      EntityPlayer player = (EntityPlayer)tickData[0];
      
      ItemStack armor = player.inventory.armorInventory[2];
      if ((armor != null) && (armor.getItem() instanceof ItemArmorBaseJetpack))
      {
        NBTTagCompound nbt = StackUtil.getOrCreateNbtData(armor);
        if (nbt.getBoolean("flight"))
        {
          if ((player.capabilities.isCreativeMode) && (!checkLastCreativeState(player)))
          {
            isLastCreativeState.put(player, Boolean.valueOf(true));
          }
          else if ((!player.capabilities.isCreativeMode) && (checkLastCreativeState(player)) && (checkFlyActiveByMode(player)))
          {
        	  player.capabilities.allowFlying = true;
        	  player.capabilities.isFlying = true;
            isLastCreativeState.put(player, Boolean.valueOf(false));
          }
          UpgradesCommonProxy.onFlyTickServer(player, armor, 0.0F, 0.0F);
          if ((player.posY > 262.0D) && (!player.capabilities.isCreativeMode)) {
        	  player.setPosition(player.posX, 262.0D, player.posZ);
          }
        }
        else if (checkFlyActiveByMode(player))
        {
        	player.capabilities.allowFlying = false;
        	player.capabilities.isFlying = false;
          isFlyActiveByMod.put(player, Boolean.valueOf(false));
          lastFlyUndressed.put(player, Boolean.valueOf(true));
        }
      }
      else if (checkFlyActiveByMode(player))
      {
    	  player.capabilities.allowFlying = false;
    	  player.capabilities.isFlying = false;
        isFlyActiveByMod.put(player, Boolean.valueOf(false));
        lastFlyUndressed.put(player, Boolean.valueOf(true));
      }
      NBTTagCompound chestnbt = getChestStored(player);
      if ((armor != null) && (IC2CA.chests.contains(armor.getItem().itemID)))
      {
        NBTTagCompound nbt = StackUtil.getOrCreateNbtData(armor);
        if (nbt.getBoolean("cloaking"))
        {
          if (checkCloakActiveByMode(player)) {
        	  player.addPotionEffect(new PotionEffect(Potion.invisibility.id, 19, 1));
          }
          UpgradesCommonProxy.onCloakTickServer(player, armor, 0.0F, 0.0F);
        }
      }
      else if ((chestnbt != null) && (chestnbt.getBoolean("has")))
      {
        int id = chestnbt.getInteger("id");
        int damage = chestnbt.getInteger("damage");
        int size = chestnbt.getInteger("size");
        ItemStack is = new ItemStack(id, size, damage);
        if (chestnbt.getCompoundTag("nbt") != null)
        {
          NBTTagCompound nbt = chestnbt.getCompoundTag("nbt");
          is.setTagCompound(nbt);
        }
        if ((is != null) && (IC2CA.chests.contains(is.getItem().itemID)))
        {
          NBTTagCompound nbt = StackUtil.getOrCreateNbtData(is);
          if (nbt.getBoolean("cloaking"))
          {
            if (checkCloakActiveByMode(player)) {
            	player.addPotionEffect(new PotionEffect(Potion.invisibility.id, 19, 1));
            }
            UpgradesCommonProxy.onCloakTickServer(player, is, 0.0F, 0.0F);
          }
        }
        else if (checkCloakActiveByMode(player))
        {
        	player.removePotionEffect(Potion.invisibility.id);
          isCloakActiveByMod.put(player, Boolean.valueOf(false));
          lastCloakUndressed.put(player, Boolean.valueOf(true));
        }
      }
    }
    if (type.contains(TickType.WORLDLOAD)) {}
  }
}

package ic2ca.common;

import ic2.api.recipe.Recipes;
import ic2.core.Ic2Items;
import ic2.core.util.StackUtil;
import ic2ca.common.item.EnumUpgradeType;
import ic2ca.common.item.ItemUpgrade;

import java.io.File;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.Configuration;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.server.FMLServerHandler;

public class ModIntegrationHandler
{
  public static final int COMPACT_SOLARS = 0;
  public static final int ADVANCED_SOLAR_PANELS = 1;
  public static final int GRAVISUITE = 2;
  public static final int GREGTECH = 3;
  private static final String[] modids = { "CompactSolars", "AdvancedSolarPanel" };
  private static boolean[] loaded = { false, false, false, false };
  private static boolean[] integrateEnabled = { true, true, true, true };
  
  /*
  private static void integrateGraviSuite()
  {
    Configuration gsconfig = getConfig(modids[2] + ".cfg");
  }
  */
  public static Configuration getConfig(String dir)
  {
    if (FMLCommonHandler.instance().getEffectiveSide().isClient()) {
    	return new Configuration(new File(Loader.instance().getConfigDir() + "/config/" + dir));    	
    }
    return new Configuration(FMLServerHandler.instance().getServer().getFile("/config/" + dir));
  }
  
  private static void integrateAdvancedSolars()
  {
    Configuration config = getConfig(modids[1] + ".cfg");
    int ashID = config.get("ITEMS", "advSolarHelmetID", 30576).getInt(30576);
    int hshID = config.get("ITEMS", "hybridSolarHelmetID", 30577).getInt(30577);
    int uhshID = config.get("ITEMS", "ultimateSolarHelmetID", 30578).getInt(30578);
    Ic2caItems.asp = new ItemStack(GameRegistry.findBlock(modids[1], "BlockAdvSolarPanel"), 1, 0);
    Ic2caItems.hybridsp = new ItemStack(GameRegistry.findBlock(modids[1], "BlockAdvSolarPanel"), 1, 1);
    Ic2caItems.ulthybsp = new ItemStack(GameRegistry.findBlock(modids[1], "BlockAdvSolarPanel"), 1, 2);
/*    Ic2caItems.ash = new ItemStack(GameRegistry.findItem(modids[0], "AdvancedSolarHelmet"));
    Ic2caItems.hsh = new ItemStack(GameRegistry.findItem(modids[0], "HybridSolarHelmet"));
    Ic2caItems.uhsh = new ItemStack(GameRegistry.findItem(modids[0], "UltimateSolarHelmet"));*/     
    Ic2caItems.ash = new ItemStack(ashID + 256, 1, 0);
    Ic2caItems.hsh = new ItemStack(hshID + 256, 1, 0);
    Ic2caItems.uhsh = new ItemStack(uhshID + 256, 1, 0);
    if (!loaded[0])
    {
    Ic2caItems.lvSolarModule = new ItemStack(new ItemUpgrade(IC2CA.lvSolarModuleID, "lvSolarModule", 64, 1, EnumUpgradeType.SOLARS));
    Ic2caItems.mvSolarModule = new ItemStack(new ItemUpgrade(IC2CA.mvSolarModuleID, "mvSolarModule", 64, 1, EnumUpgradeType.SOLARS));
    Ic2caItems.hvSolarModule = new ItemStack(new ItemUpgrade(IC2CA.hvSolarModuleID, "hvSolarModule", 64, 1, EnumUpgradeType.SOLARS));

    RecipeHandler.instance().addSolarRecipes(Ic2caItems.lvSolarModule);
    RecipeHandler.instance().addSolarRecipes(Ic2caItems.mvSolarModule);
    RecipeHandler.instance().addSolarRecipes(Ic2caItems.hvSolarModule);
    }
    ItemStack advanced = new ItemStack(Ic2caItems.solarNanoHelm.getItem(), 1);
    NBTTagCompound nbtadv = StackUtil.getOrCreateNbtData(advanced);
    nbtadv.setInteger("solarProd", 7);
    ItemStack hybrid = new ItemStack(Ic2caItems.solarQuantumHelm.getItem(), 1);
    NBTTagCompound nbthyb = StackUtil.getOrCreateNbtData(hybrid);
    nbthyb.setInteger("solarProd", 63);
    nbthyb.setInteger("upgradedTransfer", 4000);
    nbthyb.setInteger("transferLimit", 5000);
    ItemStack ultimate = new ItemStack(Ic2caItems.solarQuantumHelm.getItem(), 1);
    NBTTagCompound nbtult = StackUtil.getOrCreateNbtData(ultimate);
    nbtult.setInteger("solarProd", 511);
    nbtult.setInteger("upgradedTransfer", 4000);
    nbtult.setInteger("transferLimit", 5000);
    ArmorAssemblerRecipes.addAssemblyRecipe(advanced, Ic2caItems.ash, Ic2caItems.exoModule);
    ArmorAssemblerRecipes.addAssemblyRecipe(hybrid, Ic2caItems.hsh, Ic2caItems.exoModule);
    ArmorAssemblerRecipes.addAssemblyRecipe(ultimate, Ic2caItems.uhsh, Ic2caItems.exoModule);
    
    Recipes.advRecipes.addRecipe(Ic2caItems.lvSolarModule, new Object[] { "RRR", "CSC", "RRR", Character.valueOf('R'), "plateIron", Character.valueOf('C'), Ic2Items.insulatedCopperCableItem, Character.valueOf('S'), Ic2caItems.asp });
    Recipes.advRecipes.addRecipe(Ic2caItems.mvSolarModule, new Object[] { "RRR", "CSC", "RRR", Character.valueOf('R'), "plateIron", Character.valueOf('C'), Ic2Items.insulatedGoldCableItem, Character.valueOf('S'), Ic2caItems.hybridsp });
    Recipes.advRecipes.addRecipe(Ic2caItems.hvSolarModule, new Object[] { "RRR", "CSC", "RRR", Character.valueOf('R'), "plateIron", Character.valueOf('C'), Ic2Items.glassFiberCableItem, Character.valueOf('S'), Ic2caItems.ulthybsp });
  }
  
  private static void integrateCompactSolars()
  {
    Ic2caItems.lvHat = new ItemStack(GameRegistry.findItem(modids[0], "solarHatLV"));
    Ic2caItems.mvHat = new ItemStack(GameRegistry.findItem(modids[0], "solarHatMV"));
    Ic2caItems.hvHat = new ItemStack(GameRegistry.findItem(modids[0], "solarHatHV"));
    IC2CA.solars.add(Ic2caItems.lvHat.getItem().itemID);
    IC2CA.solars.add(Ic2caItems.mvHat.getItem().itemID);
    IC2CA.solars.add(Ic2caItems.hvHat.getItem().itemID);
    Ic2caItems.lvArray = new ItemStack(GameRegistry.findBlock(modids[0], "CompactSolarBlock"), 1, 0);
    Ic2caItems.mvArray = new ItemStack(GameRegistry.findBlock(modids[0], "CompactSolarBlock"), 1, 1);
    Ic2caItems.hvArray = new ItemStack(GameRegistry.findBlock(modids[0], "CompactSolarBlock"), 1, 2);
    
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarNanoHelm, Ic2caItems.exoNanoHelm, Ic2caItems.lvHat);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarNanoHelm, Ic2caItems.exoNanoHelm, Ic2caItems.mvHat);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarNanoHelm, Ic2caItems.exoNanoHelm, Ic2caItems.hvHat);
    
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarQuantumHelm, Ic2caItems.exoQuantumHelm, Ic2caItems.lvHat);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarQuantumHelm, Ic2caItems.exoQuantumHelm, Ic2caItems.mvHat);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarQuantumHelm, Ic2caItems.exoQuantumHelm, Ic2caItems.hvHat);
    
    Ic2caItems.lvSolarModule = new ItemStack(new ItemUpgrade(IC2CA.lvSolarModuleID, "lvSolarModule", 64, 1, EnumUpgradeType.SOLARS));
    Ic2caItems.mvSolarModule = new ItemStack(new ItemUpgrade(IC2CA.mvSolarModuleID, "mvSolarModule", 64, 1, EnumUpgradeType.SOLARS));
    Ic2caItems.hvSolarModule = new ItemStack(new ItemUpgrade(IC2CA.hvSolarModuleID, "hvSolarModule", 64, 1, EnumUpgradeType.SOLARS));
    
    RecipeHandler.instance().addSolarRecipes(Ic2caItems.lvSolarModule);
    RecipeHandler.instance().addSolarRecipes(Ic2caItems.mvSolarModule);
    RecipeHandler.instance().addSolarRecipes(Ic2caItems.hvSolarModule);
    
    Recipes.advRecipes.addRecipe(Ic2caItems.lvSolarModule, new Object[] { "RRR", "CSC", "RRR", Character.valueOf('R'), "plateIron", Character.valueOf('C'), Ic2Items.insulatedCopperCableItem, Character.valueOf('S'), Ic2caItems.lvArray });
    Recipes.advRecipes.addRecipe(Ic2caItems.mvSolarModule, new Object[] { "RRR", "CSC", "RRR", Character.valueOf('R'), "plateIron", Character.valueOf('C'), Ic2Items.insulatedGoldCableItem, Character.valueOf('S'), Ic2caItems.mvArray });
    Recipes.advRecipes.addRecipe(Ic2caItems.hvSolarModule, new Object[] { "RRR", "CSC", "RRR", Character.valueOf('R'), "plateIron", Character.valueOf('C'), Ic2Items.glassFiberCableItem, Character.valueOf('S'), Ic2caItems.hvArray });
  }
  
  public static void integrateMod(int i)
  {
    switch (i)
    {
    case 0: 
      integrateCompactSolars();
      break;
    case 1: 
      integrateAdvancedSolars();
      break;
    /*case 2: 
      integrateGraviSuite();*/
    }
  }
  public static boolean isModLoaded(int i)
  {
    return loaded[i];
  }
  
  public static void loadIntegrationModules()
  {
    IC2CA.ic2caLog.info("Loading Cross-Mod Integration Modules");
    int l = modids.length;
    for (int i = 0; i < l; i++)
    {
      String modid = modids[i];
      if (!integrateEnabled[i])
      {
        IC2CA.ic2caLog.info("Integration of " + modid + " has been disabled in the configs. Skipping.");
      }
      else if (Loader.isModLoaded(modid))
      {
        integrateMod(i);
        loaded[i] = true;
        IC2CA.ic2caLog.info("Successfully loaded integration for " + modid + ".");
      }
      else
      {
        IC2CA.ic2caLog.info("Failed to load integration for " + modid + ".");
      }
    }
  }
  
  public static void setIntegrationEnabled(int i, boolean b)
  {
    integrateEnabled[i] = b;
  }
}

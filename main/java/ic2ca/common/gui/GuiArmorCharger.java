package ic2ca.common.gui;

import org.lwjgl.opengl.GL11;

import ic2ca.common.container.ContainerArmorCharger;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;

public class GuiArmorCharger extends GuiContainer 
{
	public ContainerArmorCharger container;	
	private static final ResourceLocation background = new ResourceLocation("ic2ca", "textures/gui/GuiArmorCharger.png");

	public GuiArmorCharger(ContainerArmorCharger container1)
	{
		super(container1);
		this.container = container1;
	}
	
	protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(background);
		int x = (this.width - this.xSize) / 2;
		int y = (this.height - this.ySize) / 2;
		drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);
	}
	protected void drawGuiContainerForegroundLayer(int par1, int par2)
	{	  
		this.fontRenderer.drawString("Armor Charger", 8, 6, 4210752);
		this.fontRenderer.drawString("Inventory", 8, this.ySize - 96 + 2, 4210752);
	}	
}

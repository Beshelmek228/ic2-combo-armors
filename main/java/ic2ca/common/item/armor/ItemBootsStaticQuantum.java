package ic2ca.common.item.armor;

import ic2.api.item.ElectricItem;
import ic2.api.item.IElectricItem;
import ic2.api.item.IMetalArmor;
import ic2.core.IC2;
import ic2.core.util.StackUtil;
import ic2ca.common.IC2CA;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemBootsStaticQuantum extends ItemArmorElectricUtility implements IMetalArmor
{
	public static Map jumpChargeMap = new HashMap();

	public ItemBootsStaticQuantum(int id, String name, int renderIndex)
	{
		super(id, name, renderIndex, 3, 10000000, 12000, 4, false);

		MinecraftForge.EVENT_BUS.register(this);
	}

	public boolean doJump(EntityPlayer var1, ItemStack var2)
	{
		boolean flag = false;
		float var9 = jumpChargeMap.containsKey(var1) ? ((Float)jumpChargeMap.get(var1)).floatValue() : 1.0F;
		if ((ElectricItem.manager.canUse(var2, 1000)) && (var1.onGround) && (var9 < 1.0F))
		{
			var9 = 1.0F;
			ElectricItem.manager.use(var2, 1000, (EntityPlayer)null);
			flag = true;
		}
		if ((var1.motionY >= 0.0D) && (var9 > 0.0F) && (!var1.isInWater())) {
			if ((IC2.keyboard.isJumpKeyDown(var1)) && (IC2.keyboard.isBoostKeyDown(var1)))
			{
				if (var9 == 1.0F)
				{
					var1.motionX *= 3.5D;
					var1.motionZ *= 3.5D;
				}
				var1.motionY += var9 * 0.3F;
				var9 = (float)(var9 * 0.75D);
			}
			else if (var9 < 1.0F)
			{
				var9 = 0.0F;
			}
		}
		jumpChargeMap.put(var1, Float.valueOf(var9));
		return flag;
	}

	public boolean doStatic(EntityPlayer var1, ItemStack var2)
	{
		boolean stuff = false;
		NBTTagCompound var3 = StackUtil.getOrCreateNbtData(var2);
		int prod = 0;
		if (var3.getInteger("staticProd") > 0) {
			prod = var3.getInteger("staticProd");
		}
		boolean var4 = (var1.ridingEntity != null) || (var1.isInWater());
		if ((!var3.hasKey("x")) || (var4)) {
			var3.setInteger("x", (int)var1.posX);
		}
		if ((!var3.hasKey("z")) || (var4)) {
			var3.setInteger("z", (int)var1.posZ);
		}
		double var5 = Math.sqrt((var3.getInteger("x") - (int)var1.posX) * (var3.getInteger("x") - (int)var1.posX) + (var3.getInteger("z") - (int)var1.posZ) * (var3.getInteger("z") - (int)var1.posZ));
		if (var5 >= 5.0D)
		{
			var3.setInteger("x", (int)var1.posX);
			var3.setInteger("z", (int)var1.posZ);
			if ((var1.inventory.armorInventory[IC2CA.stPriority1] != null) && ((var1.inventory.armorInventory[IC2CA.stPriority1].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeStatic(var1, IC2CA.stPriority1, var5, prod))) {
				stuff = true;
			} else if ((var1.inventory.armorInventory[IC2CA.stPriority2] != null) && ((var1.inventory.armorInventory[IC2CA.stPriority2].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeStatic(var1, IC2CA.stPriority2, var5, prod))) {
				stuff = true;
			} else if ((var1.inventory.armorInventory[IC2CA.stPriority3] != null) && ((var1.inventory.armorInventory[IC2CA.stPriority3].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeStatic(var1, IC2CA.stPriority3, var5, prod))) {
				stuff = true;
			} else if ((var1.inventory.armorInventory[IC2CA.stPriority4] != null) && ((var1.inventory.armorInventory[IC2CA.stPriority4].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeStatic(var1, IC2CA.stPriority4, var5, prod))) {
				stuff = true;
			} else {
				stuff = false;
			}
		}
		else
		{
			stuff = false;
		}
		return stuff;
	}  

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		return "ic2ca:textures/armor/quantumstatic_1.png";
	}

	@Override
	public double getDamageAbsorptionRatio()
	{
		return 0.9D;
	}

	@Override
	public int getEnergyPerDamage()
	{
		return 800;
	}

	@Override
	public int getItemTier()
	{
		return 4;
	}

	@Override
	public ArmorProperties getProperties(EntityLivingBase var1, ItemStack var2, DamageSource var3, double var4, int var6)
	{
		if (var3 == DamageSource.fall)
		{
			int var7 = getEnergyPerDamage();
			int var8 = var7 > 0 ? 25 * ElectricItem.manager.discharge(var2, 2147483647, 2147483647, true, true) / var7 : 0;
			return new ArmorProperties(10, var4 < 8.0D ? 1.0D : 0.875D, var8);
		}
		return super.getProperties(var1, var2, var3, var4, var6);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack var1)
	{
		return EnumRarity.rare;
	}

	@Override
	public boolean isMetalArmor(ItemStack var1, EntityPlayer var2)
	{
		return true;
	}

	@Override
	public void onArmorTickUpdate(World var0, EntityPlayer player, ItemStack itemstack)
	{
		boolean didSomething = false;
		if (doStatic(player, itemstack)) {
			didSomething = true;
		}
		if (doJump(player, itemstack)) {
			didSomething = true;
		}
		if (didSomething) {
			player.inventoryContainer.detectAndSendChanges();
		}
	}

	@ForgeSubscribe
	public void onEntityLivingFallEvent(LivingFallEvent event)
	{
		if ((IC2.platform.isSimulating()) && ((event.entity instanceof EntityLivingBase)))
		{
			EntityLivingBase entity = (EntityLivingBase)event.entity;
			ItemStack armor = entity.getCurrentItemOrArmor(1);
			//		ItemStack armor = entity.inventory.armorInventory[0];	      
			if ((armor != null) && (armor.getItem() == this))
			{
				int fallDamage = (int)event.distance - 3;
				if (fallDamage >= 8) {
					return;
				}
				int energyCost = getEnergyPerDamage() * fallDamage;
				if (energyCost <= ElectricItem.manager.getCharge(armor))
				{
					ElectricItem.manager.discharge(armor, energyCost, 2147483647, true, false);

					event.setCanceled(true);
				}
			}
		}
	}
}

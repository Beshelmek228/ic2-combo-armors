package ic2ca.common.item.armor;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import ic2.api.item.ElectricItem;
import ic2.api.item.IElectricItem;
import ic2.api.item.IMetalArmor;
import ic2.api.item.Items;
import ic2.core.IC2;
import ic2.core.IC2Potion;
import ic2.core.Ic2Items;
import ic2.core.block.generator.tileentity.TileEntitySolarGenerator;
import ic2.core.item.ItemTinCan;
import ic2.core.util.StackUtil;
import ic2ca.common.IC2CA;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemHelmetQuantumSolar extends ItemArmorElectricUtility implements IMetalArmor
{
	private static final Map<Integer, Integer> potionRemovalCost = new HashMap();
	public ItemHelmetQuantumSolar(int id, String name, int renderIndex)
	{
		super(id, name, renderIndex, 0, 10000000, 12000, 4, false);
	    potionRemovalCost.put(Integer.valueOf(Potion.poison.id), Integer.valueOf(10000));
	    potionRemovalCost.put(Integer.valueOf(IC2Potion.radiation.id), Integer.valueOf(10000));
	    potionRemovalCost.put(Integer.valueOf(Potion.wither.id), Integer.valueOf(25000));
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		return "ic2ca:textures/armor/solarquantum_1.png";
	}

	@Override
	public double getDamageAbsorptionRatio()
	{
		return 1.0D;
	}

	@Override
	public int getEnergyPerDamage()
	{
		return 900;
	}

	@Override
	public int getItemEnchantability()
	{
		return 0;
	}

	@Override
	public int getItemTier()
	{
		return 4;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack var1)
	{
		return EnumRarity.rare;
	}

	@Override
	public boolean isMetalArmor(ItemStack var1, EntityPlayer var2)
	{
		return true;
	}

	@Override
	public void onArmorTickUpdate(World world, EntityPlayer player, ItemStack itemstack)
	{
		boolean didSomething = false;
		if (solarPanel(player, itemstack)) {
			didSomething = true;
		}
		if (qHelm(player, itemstack)) {
			didSomething = true;
		}
		if (didSomething) {
			player.inventoryContainer.detectAndSendChanges();
		}
	}

	public boolean qHelm(EntityPlayer player, ItemStack itemStack)
	{
		boolean ret = false;
		int air = player.getAir();
		if ((ElectricItem.manager.canUse(itemStack, 1000)) && (air < 100))
		{
			player.setAir(air + 200);
			ElectricItem.manager.use(itemStack, 1000, null);
			ret = true;
		}
		else if (air <= 0)
		{
			IC2.achievements.issueAchievement(player, "starveWithQHelmet");
		}
		if ((ElectricItem.manager.canUse(itemStack, 1000)) && (player.getFoodStats().needFood()))
		{
			int slot = -1;
			for (int i = 0; i < player.inventory.mainInventory.length; i++) {
				if ((player.inventory.mainInventory[i] != null) && (player.inventory.mainInventory[i].itemID == Ic2Items.filledTinCan.itemID))
				{
					slot = i;
					break;
				}
			}
			if (slot > -1)
			{
				ItemTinCan can = (ItemTinCan)player.inventory.mainInventory[slot].getItem();
				player.getFoodStats().addStats(can.getHealAmount(), can.getSaturationModifier());
				can.func_77849_c(player.inventory.mainInventory[slot], player.worldObj, player);
				can.onEaten(player);
				if (--player.inventory.mainInventory[slot].stackSize <= 0) {
					player.inventory.mainInventory[slot] = null;
				}
				ElectricItem.manager.use(itemStack, 1000, null);
				ret = true;
			}
		}
		else if (player.getFoodStats().getFoodLevel() <= 0)
		{
			IC2.achievements.issueAchievement(player, "starveWithQHelmet");
		}
		for (PotionEffect effect : new LinkedList<PotionEffect>(player.getActivePotionEffects())) {
			int id = effect.getPotionID();
	        Integer cost = (Integer)potionRemovalCost.get(Integer.valueOf(id));
	        if (cost != null)
	        {
	          cost = Integer.valueOf(cost.intValue() * (effect.getAmplifier() + 1));
	          if (ElectricItem.manager.canUse(itemStack, cost.intValue()))
	          {
	            ElectricItem.manager.use(itemStack, cost.intValue(), null);
	            IC2CA.proxy.removePotion(player, id);
	          }
	        }
		}
		return ret;
	}

	private boolean solarPanel(EntityPlayer player, ItemStack itemstack)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(itemstack);

		boolean stuff = false;
		int prod;
		if (nbt.getInteger("solarProd") > 0) {
			prod = nbt.getInteger("solarProd") + 1;
		} else {
			prod = 1;
		}    
		if (!player.worldObj.isRemote && TileEntitySolarGenerator.isSunVisible(player.worldObj, (int)player.posX, (int)player.posY + 1, (int)player.posZ))
		{
			if ((player.inventory.armorInventory[IC2CA.soPriority1] != null) && ((player.inventory.armorInventory[IC2CA.soPriority1].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeSolar(player, IC2CA.soPriority1, prod))) {
				stuff = true;
			} else if ((player.inventory.armorInventory[IC2CA.soPriority2] != null) && ((player.inventory.armorInventory[IC2CA.soPriority2].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeSolar(player, IC2CA.soPriority2, prod))) {
				stuff = true;
			} else if ((player.inventory.armorInventory[IC2CA.soPriority3] != null) && ((player.inventory.armorInventory[IC2CA.soPriority3].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeSolar(player, IC2CA.soPriority3, prod))) {
				stuff = true;
			} else if ((player.inventory.armorInventory[IC2CA.soPriority4] != null) && ((player.inventory.armorInventory[IC2CA.soPriority4].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeSolar(player, IC2CA.soPriority4, prod))) {
				stuff = true;
			} else {
				stuff = false;
			}
		}
		else {
			stuff = false;
		}
		return stuff;
	}
}

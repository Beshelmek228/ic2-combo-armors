package ic2ca.common.item;

import ic2.core.util.StackUtil;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class ItemNBTHelper
{
	public static int getCharge(ItemStack var0)
	{
		NBTTagCompound var1 = StackUtil.getOrCreateNbtData(var0);
		int var2 = var1.getInteger("charge");
		return var2;
	}

	public static boolean readCloakStatus(ItemStack var0)
	{
		NBTTagCompound var1 = StackUtil.getOrCreateNbtData(var0);
		return var1.getBoolean("isCloakActive");
	}

	public static boolean readFlyStatus(ItemStack var0)
	{
		NBTTagCompound var1 = StackUtil.getOrCreateNbtData(var0);
		return var1.getBoolean("isFlyActive");
	}

	public static boolean saveCloakStatus(ItemStack var0, boolean var1)
	{
		NBTTagCompound var2 = StackUtil.getOrCreateNbtData(var0);
		var2.setBoolean("isCloakActive", var1);
		return true;
	}

	public static boolean saveFlyStatus(ItemStack var0, boolean var1)
	{
		NBTTagCompound var2 = StackUtil.getOrCreateNbtData(var0);
		var2.setBoolean("isFlyActive", var1);
		return true;
	}

	public static void setCharge(ItemStack var0, int var1)
	{
		NBTTagCompound var2 = StackUtil.getOrCreateNbtData(var0);
		var2.setInteger("charge", var1);
		System.out.println(var1);
	}
}

package ic2ca.common;

import net.minecraft.item.ItemStack;

public class Ic2caItems
{
	
	public static ItemStack armorAssembler;
	public static ItemStack armorCharger;
	public static ItemStack nanoBow;
	public static ItemStack drill;
	public static ItemStack drillBit;
	public static ItemStack solarNanoHelm;
	public static ItemStack solarQuantumHelm;
	public static ItemStack nanoBatpack;
	public static ItemStack nanoAdvBatpack;
	public static ItemStack nanoEnergypack;
	public static ItemStack nanoJetpack;
	public static ItemStack nanoUltimate;
	public static ItemStack quantumBatpack;
	public static ItemStack quantumAdvBatpack;
	public static ItemStack quantumEnergypack;
	public static ItemStack quantumJetpack;
	public static ItemStack quantumUltimate;
	public static ItemStack jetpackBatpack;
	public static ItemStack jetpackAdvBatpack;
	public static ItemStack jetpackEnergypack;
	public static ItemStack nanoStatic;
	public static ItemStack quantumStatic;
	public static ItemStack exoNanoHelm;
	public static ItemStack exoNanoChest;
	public static ItemStack exoNanoLegs;
	public static ItemStack exoNanoBoots;
	public static ItemStack exoQuantumHelm;
	public static ItemStack exoQuantumChest;
	public static ItemStack exoQuantumLegs;
	public static ItemStack exoQuantumBoots;
	public static ItemStack exoJetpack;
	public static ItemStack exoBatpack;
	public static ItemStack exoAdvBatpack;
	public static ItemStack exoEnergypack;
	public static ItemStack exoSolar;
	public static ItemStack exoStatic;
	public static ItemStack exoModule;
	public static ItemStack flightModule;
	public static ItemStack jetBooster;
	public static ItemStack solarModule;
	public static ItemStack staticModule;
	public static ItemStack cloakingModule;
	public static ItemStack overchargeModule;
	public static ItemStack energyMk2;
	public static ItemStack energyMk3;
	public static ItemStack speedBooster;
	public static ItemStack opUpgrade;
	public static ItemStack lvSolarModule;
	public static ItemStack mvSolarModule;
	public static ItemStack hvSolarModule;
	public static ItemStack lvHat;
	public static ItemStack mvHat;
	public static ItemStack hvHat;
	public static ItemStack lvArray;
	public static ItemStack mvArray;
	public static ItemStack hvArray;
	public static ItemStack asp;
	public static ItemStack hybridsp;
	public static ItemStack ulthybsp;
	public static ItemStack ash;
	public static ItemStack hsh;
	public static ItemStack uhsh;
	
}

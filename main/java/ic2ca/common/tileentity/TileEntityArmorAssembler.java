package ic2ca.common.tileentity;

import ic2.api.recipe.RecipeOutput;
import ic2.core.ContainerBase;
import ic2.core.IHasGui;
import ic2.core.Ic2Items;
import ic2.core.block.invslot.InvSlotConsumableId;
import ic2.core.block.invslot.InvSlotOutput;
import ic2.core.block.machine.tileentity.TileEntityElectricMachine;
import ic2.core.util.StackUtil;
import ic2ca.common.ArmorAssemblerRecipes;
import ic2ca.common.IC2CA;
import ic2ca.common.IItemUpgrade;
import ic2ca.common.IItemUpgradeable;
import ic2ca.common.Ic2caItems;
import ic2ca.common.container.ContainerArmorAssembler;
import ic2ca.common.gui.GuiArmorAssembler;
import ic2ca.common.item.ItemUpgrade;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.StatCollector;
import cpw.mods.fml.common.ICraftingHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileEntityArmorAssembler extends TileEntityElectricMachine implements IHasGui
{
	public final InvSlotConsumableId inputSlot1;
	public final InvSlotConsumableId inputSlot2;
	public final InvSlotOutput outputSlot;
	public final InvSlotConsumableId updateSlot;	
	private ItemStack slot0;
	private ItemStack slot1;
	public int progress = 0;
	public int maxProgress = 0;
	public int timeFactor = 14;
	public int energyConsume = 32;	
	public static final int MAX_ENERGY = 50000;
	public static final int TIER = 2;
	public boolean recipeLoaded = false;
	public boolean loaded = false;

	public TileEntityArmorAssembler()
	{
		super(MAX_ENERGY, TIER, 2);

		this.inputSlot1 = new InvSlotConsumableId(this, "input1", 0, 1, ArmorAssemblerRecipes.getItemList());
		this.inputSlot2 = new InvSlotConsumableId(this, "input2", 1, 1, ArmorAssemblerRecipes.getItemList());
		this.outputSlot = new InvSlotOutput(this, "output", 3, 1);
		this.updateSlot = new InvSlotConsumableId(this, "upgrade", 4, 1, new int[] { Ic2caItems.speedBooster.itemID, Ic2caItems.opUpgrade.itemID });
	}	

	private boolean canCombine()
	{
		if ((this.inputSlot1.get() == null) || (this.inputSlot2.get() == null)) {
			return false;
		}
		RecipeOutput out = ArmorAssemblerRecipes.assembly().getAssemblyResult(this.inputSlot1.get(), this.inputSlot2.get());
		if (out == null) {
			return false;
		}
		ItemStack var1 = out.items.get(0);
		if (var1 == null) {
			return false;
		}
		if (this.outputSlot.get() == null) {
			return true;
		}
		if (!this.outputSlot.get().isItemEqual(var1)) {
			return false;
		}
		int result = this.outputSlot.get().stackSize + var1.stackSize;
		return (result <= getInventoryStackLimit()) && (result <= var1.getMaxStackSize());
	}

	public void combineItem()
	{
		if (canCombine() && !this.worldObj.isRemote)
		{
			ItemStack outputqqq = ArmorAssemblerRecipes.assembly().getAssemblyResult(this.inputSlot1.get(), this.inputSlot2.get()).items.get(0);

			ItemStack output = outputqqq.copy();

			ICraftingHandler test = IC2CA.instance;
			test.onCrafting(null, output, this);

			ItemStack in1 = this.inputSlot1.get();
			ItemStack in2 = this.inputSlot2.get();
			int charge = 0;
			NBTTagCompound nbtout = StackUtil.getOrCreateNbtData(output);
			NBTTagCompound nbtin1 = StackUtil.getOrCreateNbtData(in1);
			NBTTagCompound nbtin2 = StackUtil.getOrCreateNbtData(in2);
			charge += nbtin1.getInteger("charge");
			charge += nbtin2.getInteger("charge");
			if (charge > nbtout.getInteger("maxCharge")) {
				charge = nbtout.getInteger("maxCharge");
			}
			nbtout.setInteger("charge", charge);
			IC2CA.instance.updateElectricDamageBars(output);
			if (this.outputSlot.get() == null) {
				this.outputSlot.add(output);
			} else if (this.outputSlot.get().isItemEqual(output)) {
				this.outputSlot.get().stackSize += output.stackSize;
			}
			this.inputSlot1.get().stackSize -= 1;
			if (this.inputSlot1.get().stackSize <= 0) {
				this.inputSlot1.clear();
			}
			this.inputSlot2.get().stackSize -= 1;
			if (this.inputSlot2.get().stackSize <= 0) {
				this.inputSlot2.clear();
			}
			this.recipeLoaded = false;
		}
	}

	public String getEnergyString()
	{
		return "" + (int)(this.getChargeLevel() * 100) + "%";
	}		

	@Override
	@SideOnly(Side.CLIENT)
	public GuiScreen getGui(EntityPlayer player, boolean isAdmin)
	{
		return new GuiArmorAssembler(new ContainerArmorAssembler(player, this));
	}

	@Override
	public ContainerBase getGuiContainer(EntityPlayer player)
	{
		return new ContainerArmorAssembler(player, this);
	}

	@Override
	public String getInvName()
	{
		return "Armor Assembler";
	}

	public void getMaxProgress()
	{
		RecipeOutput out = ArmorAssemblerRecipes.assembly().getAssemblyResult(this.inputSlot1.get(), this.inputSlot2.get());
		ItemStack result = null;
		if (out != null) 
		{
			result = out.items.get(0);
		}
		if (result != null)
		{
			boolean var1 = false;
			int upgradeSlot = -1;
			if ((this.inputSlot1.get().getItem() instanceof IItemUpgrade))
			{
				var1 = true;
				upgradeSlot = 1;
			}
			else if (this.inputSlot1.get().isItemEqual(Ic2Items.overclockerUpgrade))
			{
				var1 = true;
				upgradeSlot = 1;
			}
			else if (this.inputSlot1.get().isItemEqual(Ic2Items.energyStorageUpgrade))
			{
				var1 = true;
				upgradeSlot = 1;
			}
			else if (this.inputSlot1.get().isItemEqual(Ic2Items.transformerUpgrade))
			{
				var1 = true;
				upgradeSlot = 1;
			}

			if ((this.inputSlot2.get().getItem() instanceof IItemUpgrade))
			{
				var1 = true;
				upgradeSlot = 2;
			}
			else if (this.inputSlot2.get().isItemEqual(Ic2Items.overclockerUpgrade))
			{
				var1 = true;
				upgradeSlot = 2;
			}
			else if (this.inputSlot2.get().isItemEqual(Ic2Items.energyStorageUpgrade))
			{
				var1 = true;
				upgradeSlot = 2;
			}
			else if (this.inputSlot2.get().isItemEqual(Ic2Items.transformerUpgrade))
			{
				var1 = true;
				upgradeSlot = 2;
			}

			if (var1)
			{
				IItemUpgradeable item = (IItemUpgradeable)result.getItem();
				int modifier = 1;
				if ((upgradeSlot == 1) && (this.inputSlot1.get().getItem() instanceof ItemUpgrade))
				{
					ItemUpgrade upg = (ItemUpgrade)this.inputSlot1.get().getItem();
					modifier = upg.getStackModifier(this.inputSlot1.get().getItem());
				}
				if ((upgradeSlot == 2) && (this.inputSlot2.get().getItem() instanceof ItemUpgrade))
				{
					ItemUpgrade upg = (ItemUpgrade)this.inputSlot2.get().getItem();
					modifier = upg.getStackModifier(this.inputSlot2.get().getItem());
				}				
				int tier = item.getItemTier();
				int mins = tier * 5;
				int secs = mins * 60;
				int ticks = secs * 20;
				int eu = ticks * this.timeFactor;
				int num = 0;
				if (upgradeSlot == 1) {
					num = this.inputSlot1.get().stackSize * modifier;
				}
				if (upgradeSlot == 2) {
					num = this.inputSlot1.get().stackSize * modifier;
				}				
				double min = num * 1.875D;
				double sec = min * 60.0D;
				double tick = sec * 20.0D;
				int eu2 = (int)tick * this.timeFactor;

				this.maxProgress = (eu + eu2);
			}
			else
			{
				IItemUpgradeable item = (IItemUpgradeable)result.getItem();
				int tier = item.getItemTier();
				int mins = tier * 10;
				int secs = mins * 60;
				int ticks = secs * 20;
				int eu = ticks * this.timeFactor;
				this.maxProgress = eu;
			}
			if (this.updateSlot.get() != null && this.updateSlot.get().isItemEqual(Ic2caItems.speedBooster)) 
			{
				this.maxProgress = this.maxProgress / (this.updateSlot.get().stackSize + 1);
			}
		} else {
			this.maxProgress = 0;
		}
	}

	public int getProgressScaled()
	{
		getMaxProgress();
		if (this.maxProgress == 0) {
			return 0;
		}
		return this.progress * 24 / this.maxProgress;
	}

	public String getProgressString()
	{
		int i = this.maxProgress / 100;
		if (i <= 0) {
			return "0%";
		}
		return "" + this.progress / i + "%";
	}

	public String getTimeString()
	{
		if (this.maxProgress <= 0) {
			return String.format(StatCollector.translateToLocal("info.remaining"), "0:00:00");
		}
		int mtime = this.maxProgress / 280;
		int time = this.progress / 280;
		int timeleft = mtime - time;
		int minutes = timeleft / 60;
		int hours = minutes / 60;
		minutes -= hours * 60;
		int seconds = timeleft - (minutes * 60 + hours * 3600);
		String s;
		if (seconds < 10) {
			s = ":0";
		} else {
			s = ":";
		}
		String s2;
		if (minutes < 10) {
			s2 = ":0";
		} else {
			s2 = ":";
		}
		return String.format(StatCollector.translateToLocal("info.remaining"), "" + hours + s2 + minutes + s + seconds);
	}

	@Override
	public float getWrenchDropRate()
	{
		return 0.8F;
	}

	public boolean isCombining()
	{
		return this.progress > 0;
	}

	@Override
	public boolean isInvNameLocalized()
	{
		return true;
	}

	@Override
	public void onGuiClosed(EntityPlayer entityPlayer) {}

	@Override
	public void onInventoryChanged()
	{
		super.onInventoryChanged();
		if (this.slot0 != this.inputSlot1.get() || this.slot1 != this.inputSlot2.get())
		{
			this.progress = 0;			
			this.slot0 = this.inputSlot1.get();
			this.slot1 = this.inputSlot2.get();
			this.recipeLoaded = false;
		}
		if (canCombine()) {
			getMaxProgress();
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound par1NBTTagCompound)
	{
		super.readFromNBT(par1NBTTagCompound);
		this.progress = par1NBTTagCompound.getInteger("progress");
		this.recipeLoaded = par1NBTTagCompound.getBoolean("recipeLoaded");
		this.maxProgress = par1NBTTagCompound.getInteger("maxProgress");
	}

	@Override
	public void updateEntity()
	{
		super.updateEntity();

		boolean var1 = isCombining();
		boolean needsInvUpdate = false;
		if (!this.loaded)
		{
			this.slot0 = this.inputSlot1.get();
			this.slot1 = this.inputSlot2.get();
			getMaxProgress();
			this.loaded = true;
		}
		if (this.energy >= this.energyConsume && isCombining()) {
			this.energy -= this.energyConsume;
		}
		if (!this.worldObj.isRemote)
		{
			if (this.updateSlot.get() != null && (this.updateSlot.get().isItemEqual(Ic2caItems.opUpgrade) && (canCombine())))
			{
				this.progress = 0;
				combineItem();
				needsInvUpdate = true;
			}

			if (this.energy > 0 && canCombine())
			{
				if (!this.recipeLoaded)
				{
					getMaxProgress();
					this.recipeLoaded = true;
				}
				this.progress += this.timeFactor;
				if (this.progress >= this.maxProgress)
				{
					this.progress = 0;
					this.energy -= this.energyConsume;

					combineItem();
					needsInvUpdate = true;
				}
			}
			else
			{
				this.progress = 0;
			}
			if (var1 != isCombining()) {
				needsInvUpdate = true;
			}
		}
		if (needsInvUpdate) {
			onInventoryChanged();
		}
	}	

	@Override
	public void writeToNBT(NBTTagCompound par1NBTTagCompound)
	{
		super.writeToNBT(par1NBTTagCompound);
		par1NBTTagCompound.setInteger("progress", this.progress);
		par1NBTTagCompound.setBoolean("recipeLoaded", this.recipeLoaded);
		par1NBTTagCompound.setInteger("maxProgress", this.maxProgress);
	}
}

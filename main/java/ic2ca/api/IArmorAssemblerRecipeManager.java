package ic2ca.api;

import ic2.api.recipe.IRecipeInput;
import ic2.api.recipe.RecipeOutput;

import java.util.Map;

import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public interface IArmorAssemblerRecipeManager {

	public void addRecipe(ItemStack output, IRecipeInput container, IRecipeInput fill);

	public RecipeOutput getAssemblyResult(ItemStack item1, ItemStack item2);	

	public Map<Input, RecipeOutput> getRecipes();

	public static class Input {
		public Input(IRecipeInput inp1, IRecipeInput inp2) {
			this.i1 = inp1;
			this.i2 = inp2;
		}

		public boolean matches(ItemStack it1, ItemStack it2) {
			return ((this.i1.getInputs().get(0).getItem().itemID == it1.itemID) && (this.i2.getInputs().get(0).getItem().itemID == it2.itemID));
		}

		public final IRecipeInput i1;
		public final IRecipeInput i2;
	}
}

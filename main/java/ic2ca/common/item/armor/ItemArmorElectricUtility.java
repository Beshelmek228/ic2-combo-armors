package ic2ca.common.item.armor;

import ic2.api.item.ElectricItem;
import ic2.api.item.IElectricItem;
import ic2.core.util.StackUtil;
import ic2ca.common.IC2CA;
import ic2ca.common.IItemUpgradeable;
import ic2ca.common.Util;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.StatCollector;
import net.minecraftforge.common.ISpecialArmor;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public abstract class ItemArmorElectricUtility extends ItemArmor implements ISpecialArmor, IElectricItem, IItemUpgradeable
{
	public int defaultMaxCharge;
	public int transferLimit;
	public int tier;
	public boolean share;

	public ItemArmorElectricUtility(int id, String name, int renderIndex, int piece, int maxCharge, int transferLimit, int tier, boolean share)
	{
		super(id, EnumArmorMaterial.DIAMOND, renderIndex, piece);
		this.tier = tier;
		this.defaultMaxCharge = maxCharge;
		this.transferLimit = transferLimit;
		this.share = share;
		this.setMaxDamage(27);
		this.setMaxStackSize(1);
		this.setUnlocalizedName(name);
		this.setCreativeTab(IC2CA.tabIC2CA);
		GameRegistry.registerItem(this, name);    
	}

	@Override
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(par1ItemStack);
		if (!nbt.getBoolean("loaded"))
		{
			if (nbt.getInteger("tier") == 0) 
			{
				nbt.setInteger("tier", getDefaultTier());
			}
			if (nbt.getInteger("transferLimit") == 0) 
			{
				nbt.setInteger("transferLimit", getDefaultTransferLimit());
			}
			if (nbt.getInteger("maxCharge") == 0) 
			{
				nbt.setInteger("maxCharge", getDefaultMaxCharge());
			}
			nbt.setBoolean("loaded", true);
		}
		par3List.add(StatCollector.translateToLocal("info.upgrademoduleinstalled"));
		if (nbt.getBoolean("flight")) 
		{
			par3List.add(StatCollector.translateToLocal("info.flightturbineinstalled"));
		}
		if (nbt.getBoolean("cloaking")) 
		{
			par3List.add(StatCollector.translateToLocal("info.cloakingmoduleinstalled"));
		}
		if (nbt.getBoolean("overcharge")) 
		{
			par3List.add(StatCollector.translateToLocal("info.dischargemoduleinstalled"));
		}
		if (nbt.getInteger("solarProd") > 0)
		{
			int i = nbt.getInteger("solarProd") + 1;
			par3List.add(String.format(StatCollector.translateToLocal("info.solarproduces"), i));
		}
		if (nbt.getInteger("staticProd") > 0)
		{
			int i = nbt.getInteger("staticProd") + 1;
			par3List.add(String.format(StatCollector.translateToLocal("info.staticproduces"), i));
		}
		if (nbt.getInteger("transferLimit") != getDefaultTransferLimit()) 
		{
			par3List.add(String.format(StatCollector.translateToLocal("info.transferspeed"), nbt.getInteger("transferLimit")));
		}
//		if (nbt.getInteger("tier") != getDefaultTier()) {
			par3List.add(String.format(StatCollector.translateToLocal("info.powertier"), nbt.getInteger("tier")));
//		}
	}

	@Override
	public boolean canProvideEnergy(ItemStack itemstack)
	{
		return this.share;
	}

	@Override
	public void damageArmor(EntityLivingBase entity, ItemStack stack, DamageSource source, int damage, int slot)
	{
		ElectricItem.manager.discharge(stack, damage * getEnergyPerDamage(), 2147483647, true, false);
	}

	@Override
	public int getArmorDisplay(EntityPlayer player, ItemStack armor, int slot)
	{
		return (int)Math.round(20.0D * getBaseAbsorptionRatio() * getDamageAbsorptionRatio());
	}

	@Override
	public abstract String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer);

	private double getBaseAbsorptionRatio()
	{
		switch (this.armorType)
		{
		case 0: 
			return 0.15D;
			
		case 1: 
			return 0.4D;
			
		case 2: 
			return 0.3D;
			
		case 3: 
			return 0.15D;

		default:
			return 0.0D;			
		}
	}

	@Override
	public int getChargedItemId(ItemStack itemstack)
	{
		return this.itemID;
	}

	public abstract double getDamageAbsorptionRatio();

	@Override
	public int getDefaultMaxCharge()
	{
		return this.defaultMaxCharge;
	}

	@Override
	public int getDefaultTier()
	{
		return this.tier;
	}

	@Override
	public int getDefaultTransferLimit()
	{
		return this.transferLimit;
	}

	@Override
	public int getEmptyItemId(ItemStack itemstack)
	{
		return this.itemID;
	}

	public abstract int getEnergyPerDamage();

	@Override
	public boolean getIsRepairable(ItemStack var1, ItemStack var2)
	{
		return false;
	}

	@Override
	public int getItemEnchantability()
	{
		return 0;
	}

	public abstract int getItemTier();

	@Override
	public int getMaxCharge(ItemStack itemstack)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(itemstack);
		if (nbt.getInteger("maxCharge") == 0) {
			nbt.setInteger("maxCharge", getDefaultMaxCharge());
		}
		return nbt.getInteger("maxCharge");
	}

	@Override
	public int getMaxUpgradeableCharge()
	{
		return IC2CA.maxEnergyUpgrades - getDefaultMaxCharge();
	}

	@Override
	public int getMaxUpgradeableTransfer()
	{
		return IC2CA.maxTransferUpgrades - getDefaultTransferLimit();
	}

	@Override
	public ArmorProperties getProperties(EntityLivingBase player, ItemStack armor, DamageSource source, double damage, int slot)
	{
		if (source.isUnblockable()) {
			return new ArmorProperties(0, 0.0D, 0);
		}
		double var7 = getBaseAbsorptionRatio() * getDamageAbsorptionRatio();
		int var9 = getEnergyPerDamage();
		long var10 = var9 > 0 ? 25L * ElectricItem.manager.discharge(armor, 2147483647, 2147483647, true, true) / var9 : 0L;
		int test = (int)var10;
		return new ArmorProperties(0, var7, test);
	}

	@Override
	public void getSubItems(int id, CreativeTabs var2, List itemList)
	{
		ItemStack charged = new ItemStack(this, 1);
		ElectricItem.manager.charge(charged, 2147483647, 2147483647, true, false);
		itemList.add(charged);
		itemList.add(new ItemStack(this, 1, getMaxDamage()));
	}

	@Override
	public int getTier(ItemStack itemstack)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(itemstack);
		if (nbt.getInteger("tier") == 0) {
			nbt.setInteger("tier", getDefaultTier());
		}
		return nbt.getInteger("tier");
	}

	@Override
	public int getTransferLimit(ItemStack itemstack)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(itemstack);
		if (nbt.getInteger("transferLimit") == 0) {
			nbt.setInteger("transferLimit", getDefaultTransferLimit());
		}
		return nbt.getInteger("transferLimit");
	}

	@Override
	public boolean isRepairable()
	{
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister ir)
	{
		this.itemIcon = Util.register(ir, this);
	}
}

package ic2ca.common.item.armor;

import net.minecraft.entity.Entity;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemBodyJetpackEnergypack extends ItemArmorBaseJetpack
{
	public ItemBodyJetpackEnergypack(int id, String name, int renderIndex)
	{
		super(id, name, renderIndex, 1, 2100000, 1000, 4, true);
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		return "ic2ca:textures/armor/energyjetpack_1.png";
	}

	@Override
	public int getItemTier()
	{
		return 4;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack var1)
	{
		return EnumRarity.uncommon;
	}
}

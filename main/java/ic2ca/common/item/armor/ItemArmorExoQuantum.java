package ic2ca.common.item.armor;

import ic2.api.item.ElectricItem;
import ic2.api.item.IMetalArmor;
import ic2.api.item.Items;
import ic2.core.IC2;
import ic2.core.IC2Potion;
import ic2.core.Ic2Items;
import ic2.core.item.ItemTinCan;
import ic2.core.util.StackUtil;
import ic2ca.common.IC2CA;
import ic2ca.common.Ic2caItems;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.ISpecialArmor.ArmorProperties;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemArmorExoQuantum extends ItemArmorElectricUtility implements IMetalArmor
{
	public static Map<EntityPlayer, Integer> speedTickerMap = new HashMap<EntityPlayer, Integer>();
	public static Map<EntityPlayer, Boolean> onGroundMap = new HashMap();
	public static Map<EntityPlayer, Boolean> enableQuantumSpeedOnSprintMap = new HashMap<EntityPlayer, Boolean>();
	private float jumpCharge;
	private static final Map<Integer, Integer> potionRemovalCost = new HashMap();
	//	private boolean isJumping = false;

	public ItemArmorExoQuantum(int id, String name, int renderIndex, int piece)
	{
		super(id, name, renderIndex, piece, 10000000, 12000, 4, false);
		if (piece == 4) 
		{
			MinecraftForge.EVENT_BUS.register(this);
		}
		potionRemovalCost.put(Integer.valueOf(Potion.poison.id), Integer.valueOf(10000));
		potionRemovalCost.put(Integer.valueOf(IC2Potion.radiation.id), Integer.valueOf(10000));
		potionRemovalCost.put(Integer.valueOf(Potion.wither.id), Integer.valueOf(25000));
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		if ((stack.itemID == Ic2caItems.exoQuantumHelm.itemID) || (stack.itemID == Ic2caItems.exoQuantumChest.itemID) || (stack.itemID == Ic2caItems.exoQuantumBoots.itemID)) {
			return "ic2ca:textures/armor/exoquantum_1.png";
		}
		return "ic2ca:textures/armor/exoquantum_2.png";
	}

	@Override
	public double getDamageAbsorptionRatio()
	{
		return this.armorType == 1 ? 1.1D : 1.0D;
	}	

	@Override
	public int getEnergyPerDamage()
	{
		return 900;
	}

	@Override
	public int getItemTier()
	{
		return 4;
	}

	@Override
	public ArmorProperties getProperties(EntityLivingBase entity, ItemStack armor, DamageSource source, double damage, int slot)	
	{
		if ((source == DamageSource.fall) && (this.armorType == 3))
		{
			int energyPerDamage = getEnergyPerDamage();
			int damageLimit = energyPerDamage > 0 ? 25 * ElectricItem.manager.discharge(armor, 2147483647, 2147483647, true, true) / energyPerDamage : 0;
			return new ArmorProperties(10, 1.0D, damageLimit);
		}
		return super.getProperties(entity, armor, source, damage, slot);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack var1)
	{
		return EnumRarity.rare;
	}

	@Override
	public boolean isMetalArmor(ItemStack var1, EntityPlayer var2)
	{
		return true;
	}

	@Override
	public void onArmorTickUpdate(World world, EntityPlayer player, ItemStack itemStack)
	{
		NBTTagCompound nbtData = StackUtil.getOrCreateNbtData(itemStack);		
		byte toggleTimer = nbtData.getByte("toggleTimer");
		boolean ret = false;
		switch (this.armorType)
		{
		case 0: 
			int air = player.getAir();
			if ((ElectricItem.manager.canUse(itemStack, 1000)) && (air < 100))
			{
				player.setAir(air + 200);
				ElectricItem.manager.use(itemStack, 1000, null);
				ret = true;
			}
			else if (air <= 0)
			{
				IC2.achievements.issueAchievement(player, "starveWithQHelmet");
			}
			if ((ElectricItem.manager.canUse(itemStack, 1000)) && (player.getFoodStats().needFood()))
			{
				int slot = -1;
				for (int i = 0; i < player.inventory.mainInventory.length; i++) {
					if ((player.inventory.mainInventory[i] != null) && (player.inventory.mainInventory[i].itemID == Ic2Items.filledTinCan.itemID))
					{
						slot = i;
						break;
					}
				}
				if (slot > -1)
				{
					ItemTinCan can = (ItemTinCan)player.inventory.mainInventory[slot].getItem();
					player.getFoodStats().addStats(can.getHealAmount(), can.getSaturationModifier());
					can.func_77849_c(player.inventory.mainInventory[slot], player.worldObj, player);
					can.onEaten(player);
					if (--player.inventory.mainInventory[slot].stackSize <= 0) {
						player.inventory.mainInventory[slot] = null;
					}
					ElectricItem.manager.use(itemStack, 1000, null);
					ret = true;
				}
			}
			else if (player.getFoodStats().getFoodLevel() <= 0)
			{
				IC2.achievements.issueAchievement(player, "starveWithQHelmet");
			}
			for (PotionEffect effect : new LinkedList<PotionEffect>(player.getActivePotionEffects())) {
				int id = effect.getPotionID();
				Integer cost = (Integer)potionRemovalCost.get(Integer.valueOf(id));
				if (cost != null)
				{
					cost = Integer.valueOf(cost.intValue() * (effect.getAmplifier() + 1));
					if (ElectricItem.manager.canUse(itemStack, cost.intValue()))
					{
						ElectricItem.manager.use(itemStack, cost.intValue(), null);
						IC2CA.proxy.removePotion(player, id);
					}
				}
			}
			boolean Nightvision = nbtData.getBoolean("Nightvision");
			short hubmode = nbtData.getShort("HudMode");
			if ((IC2.keyboard.isAltKeyDown(player)) && (IC2.keyboard.isModeSwitchKeyDown(player)) && (toggleTimer == 0))
			{
				toggleTimer = 10;
				Nightvision = !Nightvision;
				if (IC2.platform.isSimulating())
				{
					nbtData.setBoolean("Nightvision", Nightvision);
					if (Nightvision) {
						IC2.platform.messagePlayer(player, "Nightvision enabled.", new Object[0]);
					} else {
						IC2.platform.messagePlayer(player, "Nightvision disabled.", new Object[0]);
					}
				}
			}
			if ((IC2.keyboard.isAltKeyDown(player)) && (IC2.keyboard.isHudModeKeyDown(player)) && (toggleTimer == 0))
			{
				toggleTimer = 10;
				if (hubmode == 2) {
					hubmode = 0;
				} else {
					hubmode = (short)(hubmode + 1);
				}
				if (IC2.platform.isSimulating())
				{
					nbtData.setShort("HudMode", hubmode);
					switch (hubmode)
					{
					case 0: 
						IC2.platform.messagePlayer(player, "HUD disabled.", new Object[0]);
						break;
					case 1: 
						IC2.platform.messagePlayer(player, "HUD (basic) enabled.", new Object[0]);
						break;
					case 2: 
						IC2.platform.messagePlayer(player, "HUD (extended) enabled", new Object[0]);
					}
				}
			}
			if ((IC2.platform.isSimulating()) && (toggleTimer > 0))
			{
				toggleTimer = (byte)(toggleTimer - 1);

				nbtData.setByte("toggleTimer", toggleTimer);
			}
			if (Nightvision) {
				if (ElectricItem.manager.use(itemStack, 1, player))
				{
					int x = MathHelper.floor_double(player.posX);
					int z = MathHelper.floor_double(player.posZ);
					int y = MathHelper.floor_double(player.posY);

					int skylight = player.worldObj.getBlockLightValue(x, y, z);
					if (skylight > 8)
					{
						IC2.platform.removePotion(player, Potion.nightVision.id);
						player.addPotionEffect(new PotionEffect(Potion.blindness.id, 100, 0, true));
					}
					else
					{
						IC2.platform.removePotion(player, Potion.blindness.id);
						player.addPotionEffect(new PotionEffect(Potion.nightVision.id, 300, 0, true));
					}
					ret = true;
				}
			}
			break;
		case 1: 
			player.extinguish();
			break;
		case 2: 
			boolean enableQuantumSpeedOnSprint = true;
			if (IC2.platform.isRendering()) 
			{
				enableQuantumSpeedOnSprint = IC2.enableQuantumSpeedOnSprint;
			} else if (enableQuantumSpeedOnSprintMap.containsKey(player)) 
			{
				enableQuantumSpeedOnSprint = ((Boolean)enableQuantumSpeedOnSprintMap.get(player)).booleanValue();
			}
			if ((ElectricItem.manager.canUse(itemStack, 1000)) && ((player.onGround) || (player.isInWater())) && (IC2.keyboard.isForwardKeyDown(player)) && (((enableQuantumSpeedOnSprint) && (player.isSprinting())) || ((!enableQuantumSpeedOnSprint) && (IC2.keyboard.isBoostKeyDown(player)))))
			{
				int speedTicker = speedTickerMap.containsKey(player) ? ((Integer)speedTickerMap.get(player)).intValue() : 0;
				++speedTicker;
				if (speedTicker >= 10)
				{
					speedTicker = 0;
					ElectricItem.manager.use(itemStack, 1000, null);
					ret = true;
				}
				speedTickerMap.put(player, Integer.valueOf(speedTicker));
				float speed = 0.22F;
				if (player.isInWater())
				{
					speed = 0.1F;
					if (IC2.keyboard.isJumpKeyDown(player)) 
					{
						player.motionY += 0.10000000149011612D;
					}
				}
				if (speed > 0.0F) 
				{
					player.moveFlying(0.0F, 1.0F, speed);
				}
			}
			break;
		case 3: 
			if (IC2.platform.isSimulating())
			{
				boolean wasOnGround = onGroundMap.containsKey(player) ? ((Boolean)onGroundMap.get(player)).booleanValue() : true;
				if ((wasOnGround) && (!player.onGround) && (IC2.keyboard.isJumpKeyDown(player)) && (IC2.keyboard.isBoostKeyDown(player)))
				{
					ElectricItem.manager.use(itemStack, 4000, null);
					ret = true;
				}
				onGroundMap.put(player, Boolean.valueOf(player.onGround));
			}
			else
			{
				if ((ElectricItem.manager.canUse(itemStack, 4000)) && (player.onGround)) {
					this.jumpCharge = 1.0F;
				}
				if ((player.motionY >= 0.0D) && (this.jumpCharge > 0.0F) && (!player.isInWater())) {
					if ((IC2.keyboard.isJumpKeyDown(player)) && (IC2.keyboard.isBoostKeyDown(player)))
					{
						if (this.jumpCharge == 1.0F)
						{
							player.motionX *= 3.5D;
							player.motionZ *= 3.5D;
						}
						player.motionY += this.jumpCharge * 0.3F;
						this.jumpCharge = ((float)(this.jumpCharge * 0.75D));
					}
					else if (this.jumpCharge < 1.0F)
					{
						this.jumpCharge = 0.0F;
					}
				}
			}
		}
		if (ret) {
			player.inventoryContainer.detectAndSendChanges();
		}
	}

	@ForgeSubscribe
	public void onEntityLivingFallEvent(LivingFallEvent event)
	{
		if (IC2.platform.isSimulating() && (event.entity instanceof EntityLivingBase))
		{
			EntityLivingBase entity = (EntityLivingBase)event.entity;
			ItemStack armor = entity.getCurrentItemOrArmor(1);
			if (armor != null && armor.itemID == this.itemID)
			{
				int fallDamage = Math.max((int)event.distance - 10, 0);
				int energyCost = this.getEnergyPerDamage() * fallDamage;
				if (energyCost <= ElectricItem.manager.getCharge(armor))
				{
					ElectricItem.manager.discharge(armor, energyCost, 2147483647, true, false);
					event.setCanceled(true);
				}
			}
		}
	}

	public static void removePlayerReferences(EntityPlayer player)
	{
		speedTickerMap.remove(player);
		onGroundMap.remove(player);
		enableQuantumSpeedOnSprintMap.remove(player);
	}
}

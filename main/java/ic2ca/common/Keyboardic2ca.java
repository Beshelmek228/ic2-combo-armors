package ic2ca.common;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.entity.player.EntityPlayer;

public class Keyboardic2ca
{
  private static Map boostKeyState = new HashMap();
  private static Map jumpKeyState = new HashMap();
  private static Map sneakKeyState = new HashMap();
  
  public static boolean isBoostKeyDown(EntityPlayer player)
  {
    return boostKeyState.containsKey(player) ? ((Boolean)boostKeyState.get(player)).booleanValue() : false;
  }
  
  public static boolean isJumpKeyDown(EntityPlayer player)
  {
    return jumpKeyState.containsKey(player) ? ((Boolean)jumpKeyState.get(player)).booleanValue() : false;
  }
  
  public static boolean isSneakKeyDown(EntityPlayer player)
  {
    return sneakKeyState.containsKey(player) ? ((Boolean)sneakKeyState.get(player)).booleanValue() : false;
  }
  
  public static void processKeyUpdate(EntityPlayer player, int var1)
  {
    boostKeyState.put(player, Boolean.valueOf((var1 & 0x1) != 0));
    jumpKeyState.put(player, Boolean.valueOf((var1 & 0x2) != 0));
    sneakKeyState.put(player, Boolean.valueOf((var1 & 0x4) != 0));
  }
}

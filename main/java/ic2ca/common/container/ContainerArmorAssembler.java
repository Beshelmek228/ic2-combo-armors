package ic2ca.common.container;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import ic2.core.block.machine.ContainerElectricMachine;
import ic2.core.slot.SlotInvSlot;
import ic2ca.common.tileentity.TileEntityArmorAssembler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;

public class ContainerArmorAssembler extends ContainerElectricMachine
{
	public final TileEntityArmorAssembler tileEntity;
	private int lastProgress = 0;
	private int lastEnergy = 0;

	public ContainerArmorAssembler(EntityPlayer player, TileEntityArmorAssembler tileentity)
	{
	    super(player, tileentity, 166, 56, 53);
		this.tileEntity = tileentity;
		addSlotToContainer(new SlotInvSlot(tileentity.inputSlot1, 0, 42, 17));
		addSlotToContainer(new SlotInvSlot(tileentity.inputSlot2, 0, 68, 17));
		addSlotToContainer(new SlotInvSlot(tileentity.outputSlot, 0, 122, 35));
		addSlotToContainer(new SlotInvSlot(tileentity.updateSlot, 0, 150, 35));
		bindPlayerInventory(player.inventory);
	}

	protected void bindPlayerInventory(InventoryPlayer inventoryPlayer)
	{
		for (int i = 0; i < 3; i++) 
		{
			for (int j = 0; j < 9; j++) 
			{
				addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
			}
		}
		for (int i = 0; i < 9; i++) {
			addSlotToContainer(new Slot(inventoryPlayer, i, 8 + i * 18, 142));
		}
	}

	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();

		for (int i = 0; i < this.crafters.size(); i++)
		{
			ICrafting icrafting = (ICrafting)this.crafters.get(i);
			if (this.tileEntity.progress != this.lastProgress)
			{
				int e = (int)(this.tileEntity.progress / 50);
				int q = this.tileEntity.progress - e * 50;
				icrafting.sendProgressBarUpdate(this, 0, e);
				icrafting.sendProgressBarUpdate(this, 1, q);
			}
		}
		this.lastProgress = this.tileEntity.progress;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int index, int par2)
	{
		switch (index)
		{
		case 0: 
			this.tileEntity.progress = (par2 * 50);
			break;
		case 1: 
			this.tileEntity.progress += par2;
			break;
		case 2: 
			this.tileEntity.energy = (par2 * 50);
			break;			
		case 3: 
			this.tileEntity.energy += par2;
			break;			
		}		
	}
}

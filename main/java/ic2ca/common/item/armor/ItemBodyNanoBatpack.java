package ic2ca.common.item.armor;

import ic2.api.item.IMetalArmor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemBodyNanoBatpack extends ItemArmorElectricUtility implements IMetalArmor
{
  public ItemBodyNanoBatpack(int id, String name, int renderIndex)
  {
    super(id, name, renderIndex, 1, 2000000, 160, 3, true);
  }

  @Override
  public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
  {
    return "ic2ca:textures/armor/nanobat_1.png";
  }
  
  @Override
  public double getDamageAbsorptionRatio()
  {
    return 0.9D;
  }
  
  @Override
  public int getEnergyPerDamage()
  {
    return 800;
  }
  
  @Override
  public int getItemTier()
  {
    return 3;
  }
  
  @Override
  @SideOnly(Side.CLIENT)
  public EnumRarity getRarity(ItemStack var1)
  {
    return EnumRarity.uncommon;
  }
  
  @Override
  public boolean isMetalArmor(ItemStack var1, EntityPlayer var2)
  {
    return true;
  }
}

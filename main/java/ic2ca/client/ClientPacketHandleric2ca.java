package ic2ca.client;

import ic2.core.util.StackUtil;
import ic2ca.common.CommonPacketHandleric2ca;
import ic2ca.common.IC2CA;
import ic2ca.common.item.armor.ItemArmorBaseJetpack;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import cpw.mods.fml.common.network.Player;

public class ClientPacketHandleric2ca extends CommonPacketHandleric2ca
{
	public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player)
	{
		DataInputStream stream = new DataInputStream(new ByteArrayInputStream(packet.data));
		try
		{
			String tag = stream.readUTF();
			if (tag.equalsIgnoreCase("setFlyStatus"))
			{
				ItemStack armor = ClientProxyic2ca.mc.thePlayer.inventory.armorInventory[2];
				if (armor != null && armor.getItem() instanceof ItemArmorBaseJetpack)
				{
					NBTTagCompound nbt = StackUtil.getOrCreateNbtData(armor);
					if (nbt.getBoolean("flight"))
					{
						UpgradesClientProxy.firstLoadClient(ClientProxyic2ca.mc.thePlayer, armor);
					}
				}
				NBTTagCompound chestnbt = ClientTickHandleric2ca.chestNBT;
				if (armor != null && IC2CA.chests.contains(armor.getItem().itemID))
				{
					NBTTagCompound nbt = StackUtil.getOrCreateNbtData(armor);
					if (nbt.getBoolean("cloaking")) 
					{
						UpgradesClientProxy.firstLoadClient(ClientProxyic2ca.mc.thePlayer, armor);
					}
				}
				else if (chestnbt != null && chestnbt.getBoolean("has"))
				{
					int id = chestnbt.getInteger("id");
					int damage = chestnbt.getInteger("damage");
					int size = chestnbt.getInteger("size");
					ItemStack is = new ItemStack(id, size, damage);
					if (chestnbt.getCompoundTag("nbt") != null)
					{
						NBTTagCompound nbt = chestnbt.getCompoundTag("nbt");
						is.setTagCompound(nbt);
					}
					if (IC2CA.chests.contains(is.getItem().itemID))
					{
						NBTTagCompound nbt = StackUtil.getOrCreateNbtData(is);
						if (nbt.getBoolean("cloaking")) {
							UpgradesClientProxy.firstLoadClient(ClientProxyic2ca.mc.thePlayer, is);
						}
					}
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
